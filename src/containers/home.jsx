import React, {useState, useEffect} from 'react'
import img1 from '../assets/images/1.jpg'
import img2 from '../assets/images/2.jpg'
import img3 from '../assets/images/3.jpg'
import img4 from '../assets/images/4.jpg'
import img5 from '../assets/images/5.jpg'
import img6 from '../assets/images/6.jpg'

const Home = (props) => {
    const slides = [
        { image: img1, legend: 'Street Art'          },
        { image: img2, legend: 'Fast Lane'           },
        { image: img3, legend: 'Colorful Building'   },
        { image: img4, legend: 'Skyscrapers'         },
        { image: img5, legend: 'City by night'       },
        { image: img6, legend: 'Tour Eiffel la nuit' }
    ];
    {/* deux states timer et index */}
    
    const getRandomInteger = (min, max) =>{
	    return Math.floor(Math.random() * (max - min + 1)) + min;
    }

    const randomImg = () => {

    }

    const playPause = () => {
        
    }
    return (
        <section>
            <h2>Home Page</h2>
        </section>
    )
}

export default Home