import Header from "./components/header"
import Home from "./containers/home"
import './App.css'

function App() {

  return (
    <>
      <Header/>
      <Home/>
    </>
  )
}

export default App
